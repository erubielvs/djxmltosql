from django.shortcuts import render
# from cfdi.cfdi.models import sqlserverconn
import pyodbc


def connectsql(request):
    # conn=pyodbc.connect('Driver={SQL SERVER};'
    #                     'Server=ZAZX-LAPTOP\\SQLEXPRESS;'
    #                     'Database=PruebaDjango;'
    #                     'Trusted_Connection=yes;')
    conn = pyodbc.connect('Driver={SQL SERVER};'
                          'Server=ZAZX-LAPTOP\\SQLEXPRESS;'
                          'Database=XMLMODELO;'
                          'Trusted_Connection=yes;')
    cursor=conn.cursor()
    cursor.execute('select cm.folio,cm.serie,rc.nombre,rc.rfc,cm.total from comprobante as cm '
                   'inner join emisor as rc'
                   ' on rc.idComprobante = cm.idComprobante')
    result = cursor.fetchall()
    return render(request,'example.html',{'sqlserverconn':result})

